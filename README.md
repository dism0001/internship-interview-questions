# Internship Interview Questions

*Please include explanation along with your answers.*

1. Please describe yourself using JSON (include your internship start/end date, your current location).

{
    "Name": "Dania Ismadi",
    "Internship Start Date": "November 30, 2020",
    "Internship End Date": "February 28, 2021",
    "Current Location": "Kuala Lumpur, Malaysia"
    "Education": [
        {
            "Institution Name": "Monash University",
            "Major": "Computer Science",
            "CGPA": "3.89/4.00",
            "WAM": 88.36,
            "Relevant Coursework": ["Algorithms and Data Structures", "Software Architecture and Design", "Deep Learning"],
            "Anticipated Graduation Date": "November 2021"
        }
    ],
    "Programming Languages": ["Python", "Java", "R", "SQL", "JavaScript", "HTML/CSS", "Typescript"],
    "Databases": ["MongoDB", "Oracle", "Firebase"],
    "Version Control": ["Git"],
    "Interests": ["Tennis", "Badminton", "Video Editing", "Digital Illustration"]
}
  
2. Tell us about a newer (less than five years old) web technology you like and why?

A new web technology that I like is Flutter which is Google's portable UI toolkit for crafting natively compiled applications for mobile, web, and desktop from a single codebase. It was initially released in 2017. The reason why I love it is because it is not exclusive to one type of device like how Swift is to iOS, JavaScript is to the web and Java/Kotlin is to Android- it can be used for all three types of devices which is very convenient. The overall branding and aesthetics of Flutter is also very appealing. I have used Flutter to implement the simple coding exercise below.

3. In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that (other than using ArrayList)?

We could create our own Linked List data structure by creating the Node and LinkedList classes and implementing the required methods, like append, to add more items over time. We could also create other data structures like Trees or Graphs that would achieve the same results.

4. Explain this block of code in Big-O notation.

    ```
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
    }
    ```

O(n<sup>2</sup>), where n is size (as specified in the input of the function). This is because for each iteration of the outer for loop, the inner for loop iterates n times. The outer for loop iterates n times. Thus, the time complexity of this function is O(n<sup>2</sup>).

5. One of our developers built a page to display market information for over 500 cryptocurrencies in a single page. After several days, our support team started receiving complaints from our users that they are not able to view the website and it takes too long to load. What do you think causes this issue and suggest at least 3 ideas to improve the situation.

This might be because there are too many HTTP GET requests as users are trying to access a single webpage since the developer displays market data of 500 cryptocurrencies in a single page which is quite a lot. This puts a very heavy pressure on the server which causes the website to load for too long. To fix this, you can either:
- Change to a faster web hosting service that would be able to handle this pressure.
- You can display less information in a single page- perhaps, 50 cryptocurrencies or less market data per cryptocurrency.
- You can apply functional reactive programming to allow for asynchronous behaviours, i.e., allow the users to interact with the first few coins that are displayed on the page and load the rest of the coins as the user scrolls through the page instead of loading all 500 cryptocurrencies at once.

6. In Javascript, What is a "closure"? How does JS Closure works?

A function and the set of variables it accesses from its enclosing scope is called a closure. For example,

    ```
    function add(x) {
    return y => y+x; // we are going to return a function which includes
                     // the variable x from it’s enclosing scope 
                     // - “a closure”
    }
    ```
In the above JavaScript code, the parameter x of the add function is captured by the anonymous function that is returned, which forms a closure. The binding of 'x' to a value persists beyond the scope of the add function. We have used the add function to create a new function. For example, if we pass in the number, 9, for 'x', then we will return the function, ``` y => y + 9 ```.

7. In Javascript, what is the difference between var, let, and const. When should I use them?

```const``` are used to assign immutable variables. Use this for values that are final. This is recommended for pure, functional programming to reduce the amount of side effects.

```let``` are used to assign mutable variables (variables whose value we can change later on). However, ```let``` variables cannot be re-declared.
```var``` variables can be re-declared and updated which may cause some unwanted side effects in your code.

8. Share with us one book that has changed your perspective in life. How did it change your life?

One book that has changed my life is Sapiens: A Brief History of Mankind by Yuval Noah Harari. It changed my life as it introduced so many new perspectives to build upon my own knowledge about history, life, economics, etc. I particularly found it fascinating when he mentioned that most things that are so important to humans in today's world (i.e., money and religion), are simply myths that we, as the human race, created ourselves.

9. What is the thing you believe in that is true, that most people disagree with?

Most people believe that great leaders have to be assertive, result-oriented and to a certain extent, ruthless. Whilst I agree that those are desirable qualities to have, I believe that an empathetic and compassionate person can also be strong leaders. Most people would say that these qualities are weaknesses, but I throughly disagree. It's all about balance.

10. What are your thoughts on the subject of Arts and Humanities?

I think Arts and Humanities are very important subjects for everyone to learn, no matter what age you are and what field or background you come from. As someone who studies a very technical field (Computer Science), it can be very easy for people like me to disregard Arts and Humanities. However, from personal experience, being open to learning these subjects really helped shape me into becoming a well-rounded person overall. For example, I can approach problems from both a technical and creative perspective, I am able to communicate my ideas and opinions more fluently to my peers and finally, I am able to analyse the ethics and consequences of the programs I create and how it would affect social and/or political issues in the world.

---
# Simple Coding Assessment

Build a Cryptocurrency Market dashboard showing market data.

requirement:
1. using the API endpoints found in https://www.coingecko.com/en/api, build a cryptocurrency market dashboard page.
2. The page should be able to list at least 20 coins.
3. The page should show price, volume, name, symbol of the coin.
4. The page should show the graph of 7 days data, using the **sparkline** returned from api. For example sparkline data can be obtained using [https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true](https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true) or [https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true](https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true)
5. The page should allow user to "favorite" coins.
6. build another page "favorite", to only show the favorite coins.
5. **(bonus)** The page should allow user to switch currency for ("usd", "myr", "btc"). The price and volume should display the number in the currency selected.
6. **(bonus)** Host this on a website, or a mobile app.
7. We will schedule a video call with you should we decide to proceed with your interview.
8. You must be able to demo your submission to us.

For this coding assessment, I wrote a Flutter program using Dart to build a mobile Cryptocurrency market dashboard. The code for this can be viewed in the market_dashboard folder under /lib/main.dart. The app can be simulated using a simulator and then running "flutter run" on the terminal.

---
# Submission instruction

1. Fork this repo.
2. Creates a Merge Request in this repo after completion.
