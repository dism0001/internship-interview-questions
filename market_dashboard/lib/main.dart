import 'package:flutter/material.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Market Dashboard',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new Dashboard(title: 'Cryptocurrency Market'),
    );
  }
}

class Dashboard extends StatefulWidget {
  Dashboard({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _DashboardState createState() => new _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  // fetch the coins
  Future<List<Coin>> _getCoins() async {
    // get list of coin id's
    var data = await http.get('https://api.coingecko.com/api/v3/coins/list');
    var jsonData = json.decode(data.body);

    List<Coin> coins = [];

    // get 100 coins and its data
    for (int i = 50; i < 100; i++) {
      // get id of coin
      String id = jsonData[i]['id'];

      // get coin data from http
      var coin = await http.get('https://api.coingecko.com/api/v3/coins/' +
          id +
          '?tickers=false&market_data=true&community_data=false&developer_data=false&sparkline=true');
      var coinData = json.decode(coin.body);

      // create new coin
      Coin newCoin = Coin();
      newCoin.coinId = coinData['id'];
      newCoin.coinSymbol = coinData['symbol'];
      newCoin.coinName = coinData['name'];
      newCoin.coinPrice = coinData['market_data']['current_price'];
      newCoin.coinVolume = coinData['market_data']['total_volume'];
      newCoin.coinSparkline = coinData['market_data']['sparkline_7d']['price'];
      newCoin.icon = coinData['image']['small'];

      // add to list
      coins.add(newCoin);
    }

    return coins;
  }

  List<int> _selectedIndex = [];
  List<Coin> favCoins = [];

  _onSelected(int index) {
    setState(() => _selectedIndex.add(index));
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: new Scaffold(
          appBar: new AppBar(
              title: new Text(widget.title),
              bottom: TabBar(tabs: [
                Tab(
                  text: 'Dashboard',
                ),
                Tab(icon: Icon(Icons.favorite))
              ])),
          body: TabBarView(children: [
            Container(
                child: FutureBuilder(
                    future: _getCoins(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.data == null) {
                        return Container(
                            alignment: Alignment.center,
                            child: CircularProgressIndicator());
                      } else {
                        return ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                                child: Card(
                                    elevation: 5,
                                    child: Column(
                                      children: <Widget>[
                                        getListTile(
                                            snapshot.data,
                                            index,
                                            _selectedIndex,
                                            _onSelected,
                                            favCoins),
                                        getSparkline(snapshot.data, index),
                                      ],
                                    )));
                          },
                        );
                      }
                    })),
            Container(
                child: ListView.builder(
              itemCount: favCoins.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                    child: Card(
                        elevation: 5,
                        child: Column(children: <Widget>[
                          getFavouritesListTile(favCoins, index),
                          getSparkline(favCoins, index)
                        ])));
              },
            ))
          ])),
    );
  }
}

// get sparkline data
Widget getSparkline(snapshot, index) {
  List<dynamic> data = snapshot[index].coinSparkline;
  List<double> finalData = data.cast<double>();
  print(finalData);
  if (finalData.length > 0) {
    return Padding(
        padding: EdgeInsets.all(10.0),
        child: new Sparkline(
            data: finalData,
            lineColor: Color(0xff808080),
            pointsMode: PointsMode.all,
            pointSize: 4.0));
  } else {
    return Padding(padding: EdgeInsets.all(1.0));
  }
}

// get list tiles for favourites page
Widget getFavouritesListTile(snapshot, index) {
  return ListTile(
    leading: CircleAvatar(
      backgroundImage: NetworkImage(snapshot[index].icon),
    ),
    title: Text(snapshot[index].coinName),
    subtitle: Text('Price: ' +
        snapshot[index].coinPrice['usd'].toString() +
        ', Volume: ' +
        snapshot[index].coinVolume['usd'].toString() +
        ', Symbol: ' +
        snapshot[index].coinSymbol),
  );
}

// get list tiles for dashboard page
Widget getListTile(snapshot, index, _selectedIndex, _onSelected, favCoins) {
  return ListTile(
    leading: CircleAvatar(
      backgroundImage: NetworkImage(snapshot[index].icon),
    ),
    title: Text(snapshot[index].coinName),
    subtitle: Text('Price: ' +
        snapshot[index].coinPrice['usd'].toString() +
        ', Volume: ' +
        snapshot[index].coinVolume['usd'].toString() +
        ', Symbol: ' +
        snapshot[index].coinSymbol),
    trailing: IconButton(
      icon: Icon(Icons.favorite,
          color:
              _selectedIndex.contains(index) ? Colors.redAccent : Colors.grey),
      onPressed: () {
        if (!_selectedIndex.contains(index)) {
          favCoins.add(snapshot[index]);
          _onSelected(index);
        }
      },
    ),
  );
}

// get coin name
Widget getCoinName(Coin coin) {
  return Align(
    alignment: Alignment.centerLeft,
    child: RichText(
      text: TextSpan(
        text: coin.coinName,
        style: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.black, fontSize: 20),
        children: <TextSpan>[
          TextSpan(
              text: coin.coinSymbol,
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 15,
                  fontWeight: FontWeight.bold)),
        ],
      ),
    ),
  );
}

// get coin amount
Widget getCointAmount(Coin coin, String currency) {
  return Align(
    alignment: Alignment.centerLeft,
    child: Padding(
      padding: const EdgeInsets.only(left: 20.0),
      child: Row(
        children: <Widget>[
          RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
              text: coin.coinPrice[currency].toString(),
              style: TextStyle(
                color: Colors.grey,
                fontSize: 35,
              ),
              children: <TextSpan>[
                TextSpan(
                    text: coin.coinVolume[currency].toString(),
                    style: TextStyle(
                        color: Colors.grey,
                        fontStyle: FontStyle.italic,
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

// class for coin
class Coin {
  String coinId;
  String coinSymbol;
  String coinName;
  Map<String, dynamic> coinPrice;
  Map<String, dynamic> coinVolume;
  List<dynamic> coinSparkline = [0];
  String icon;

  Coin(
      {this.coinId,
      this.coinSymbol,
      this.coinName,
      this.coinPrice,
      this.coinVolume,
      this.coinSparkline,
      this.icon});
}
